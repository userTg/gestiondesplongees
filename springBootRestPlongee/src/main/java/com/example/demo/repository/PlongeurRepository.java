package com.example.demo.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.model.Plongeur;

public interface PlongeurRepository extends JpaRepository<Plongeur, Long> {

	@Query("select p from Plongeur p where p.nom like %:x%")
	public Page<Plongeur> chercher(@Param("x") String mc, Pageable pageable);

	Plongeur findByNum(Long num);

}
