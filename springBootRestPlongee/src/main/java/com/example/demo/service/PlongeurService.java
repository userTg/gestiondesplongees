package com.example.demo.service;

import java.util.Collection;
import java.util.Optional;

import javax.validation.Valid;

import com.example.demo.model.Plongeur;

/**
 * @author thierryguilloteau
 *
 */
public interface PlongeurService {

	Collection<Plongeur> getAllPlongeur();

	Plongeur saveOrUpdatePlongeur(Plongeur plongeurToUpdate);

	void deletePlongeur(Long num);

	Plongeur getPlongeurById(Long num);

}
