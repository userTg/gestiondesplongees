package com.example.demo.dto;
import java.io.Serializable;



/**
 * @author thierryguilloteau
 *
 */

public class PlongeurDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String prenom;
	private String nom;
	private String niveau;
	private String datePlongee;
	private int profondeur;
	private String dureePlongee;
	private String description;
	
	
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDureePlongee() {
		return dureePlongee;
	}
	public void setDureePlongee(String dureePlongee) {
		this.dureePlongee = dureePlongee;
	}
	public int getProfondeur() {
		return profondeur;
	}
	public void setProfondeur(int profondeur) {
		this.profondeur = profondeur;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getNiveau() {
		return niveau;
	}
	public void setNiveau(String niveau) {
		this.niveau = niveau;
	}
	public String getDatePlongee() {
		return datePlongee;
	}
	public void setDatePlongee(String datePlongee) {
		this.datePlongee = datePlongee;
	}
	
	public PlongeurDto(String prenom, String nom, String niveau, String datePlongee, int profondeur, String dureePlongee, String description) {
		super();
		this.prenom = prenom;
		this.nom = nom;
		this.niveau = niveau;
		this.datePlongee = datePlongee;
		this.profondeur= profondeur;
		this.dureePlongee = dureePlongee;
		this.description = description;
	}
	public PlongeurDto(Long id, String prenom, String nom, String niveau, String datePlongee, int profondeur, String dureePlongee, String description) {
		super();
		this.id = id;
		this.prenom = prenom;
		this.nom = nom;
		this.niveau = niveau;
		this.datePlongee = datePlongee;
		this.profondeur= profondeur;
		this.dureePlongee = dureePlongee;
		this.description = description;
	}
	@Override
	public String toString() {
		return "PlongeurDto [id=" + id + ", prenom=" + prenom + ", nom=" + nom + ", niveau=" + niveau + ", datePlongee="
				+ datePlongee + ", profondeur=" + profondeur + ", dureePlongee=" + dureePlongee + ", description="
				+ description + "]";
	}
	

	
	
	
	
	

}
